import 'package:shared_preferences/shared_preferences.dart';

class Preferences {

  static late SharedPreferences _prefs;

  static String _token = "";

  static Future init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  static String get getToken {
    return _prefs.getString('token') ?? _token;
  }

  static set setToken( String token ) {
    _token = token;
    _prefs.setString('token', token );
  }


}