import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gerenciar2/utils/local_storage_pref.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';

class CustomInAppBrowser extends StatefulWidget {
  final String url;
  final String token = Preferences.getToken;

  CustomInAppBrowser({Key? key, required this.url}) : super(key: key);

  @override
  State<CustomInAppBrowser> createState() => _CustomInAppBrowserState();
}

class _CustomInAppBrowserState extends State<CustomInAppBrowser> {
  final GlobalKey webViewKey = GlobalKey();
  final ReceivePort _port = ReceivePort();

  String url = '';
  String title = '';
  double progress = 0;
  bool? isSecure;
  InAppWebViewController? webViewController;

  @override
  void initState() {
    super.initState();
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      String id = data[0];
      dynamic status = data[1];
      int progress = data[2];
      if (kDebugMode) {
        print("Download progress: $progress%");
      }
      if (status == DownloadTaskStatus.complete) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Download $id completed!"),
        ));
      }
    });
    FlutterDownloader.registerCallback(downloadCallback, step: 10);
    url = widget.url;
  }

  @pragma('vm:entry-point')
  static void downloadCallback(
      String id, dynamic status, int progress) {
    final SendPort? send =
    IsolateNameServer.lookupPortByName('downloader_send_port');
    send?.send([id, status, progress]);
  }


  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    var url = '';
    var prefix = '';

    if (Platform.isAndroid) {
      prefix = "android";
    } else if (Platform.isIOS) {
      prefix = "ios";
    }

    if (widget.token.isNotEmpty) {
      url = widget.url + '/' + prefix + '-' + widget.token;
    } else {
      url = widget.url + '/' + prefix;
    }

    print("MY_URL: ${url}");




    return Scaffold(
      body: SafeArea(
        child: Column(children: <Widget>[
          Expanded(
              child: Stack(
                children: [
                  InAppWebView(
                    key: webViewKey,
                    initialUrlRequest: URLRequest(url: WebUri(url)),
                    initialSettings: InAppWebViewSettings(
                        useOnDownloadStart: true,
                        transparentBackground: true,
                        safeBrowsingEnabled: true,
                        isFraudulentWebsiteWarningEnabled: true),
                    onWebViewCreated: (controller) async {
                      webViewController = controller;
                      if (!kIsWeb &&
                          defaultTargetPlatform == TargetPlatform.android) {
                        await controller.startSafeBrowsing();
                      }
                    },
                    onLoadStart: (controller, url) {
                      if (url != null) {
                        setState(() {
                          this.url = url.toString();
                          isSecure = urlIsSecure(url);
                        });
                      }
                    },
                    onLoadStop: (controller, url) async {
                      if (url != null) {
                        setState(() {
                          this.url = url.toString();
                        });
                      }

                      final sslCertificate = await controller.getCertificate();
                      setState(() {
                        isSecure = sslCertificate != null ||
                            (url != null && urlIsSecure(url));
                      });
                    },
                    onUpdateVisitedHistory: (controller, url, isReload) {
                      if (url != null) {
                        setState(() {
                          this.url = url.toString();
                        });
                      }
                    },
                    onTitleChanged: (controller, title) {
                      if (title != null) {
                        setState(() {
                          this.title = title;
                        });
                      }
                    },
                    onProgressChanged: (controller, progress) {
                      setState(() {
                        this.progress = progress / 100;
                      });
                    },
                    shouldOverrideUrlLoading: (controller, navigationAction) async {
                      if (!kIsWeb && defaultTargetPlatform == TargetPlatform.iOS) {
                        final shouldPerformDownload =
                            navigationAction.shouldPerformDownload ?? false;
                        final url = navigationAction.request.url;
                        if (shouldPerformDownload && url != null) {
                          await downloadFile(url.toString());
                          return NavigationActionPolicy.DOWNLOAD;
                        }
                      }
                      return NavigationActionPolicy.ALLOW;
                    },
                    onDownloadStartRequest: (controller, downloadStartRequest) async {
                      await downloadFile(downloadStartRequest.url.toString(),
                          downloadStartRequest.suggestedFilename);
                    },
                  ),

                  progress < 1.0
                      ? LinearProgressIndicator(value: progress)
                      : Container(),
                ],
              )),
        ]),
      ),
    );
  }

  Future<void> downloadFile(String url, [String? filename]) async {
    var hasStoragePermission = await Permission.storage.isGranted;
    if (!hasStoragePermission) {
      final status = await Permission.storage.request();
      hasStoragePermission = status.isGranted;
    }
    if (hasStoragePermission) {
      final taskId = await FlutterDownloader.enqueue(
          url: url,
          headers: {},
          // optional: header send with url (auth token etc)
          savedDir: (await getTemporaryDirectory()).path,
          saveInPublicStorage: true,
          openFileFromNotification: true,
          fileName: filename);
    }
  }



  void handleClick(int item) async {
    switch (item) {
      case 0:
        await InAppBrowser.openWithSystemBrowser(url: WebUri(url));
        break;
      case 1:
        await webViewController?.clearCache();
        if (!kIsWeb && defaultTargetPlatform == TargetPlatform.android) {
          await webViewController?.clearHistory();
        }
        setState(() {});
        break;
    }
  }

  static bool urlIsSecure(Uri url) {
    return (url.scheme == "https") || isLocalizedContent(url);
  }

  static bool isLocalizedContent(Uri url) {
    return (url.scheme == "file" ||
        url.scheme == "chrome" ||
        url.scheme == "data" ||
        url.scheme == "javascript" ||
        url.scheme == "about");
  }
}