import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:gerenciar2/services/push_noticatios_service.dart';
import 'package:gerenciar2/utils/local_storage_pref.dart';
import './utils/constant.dart' as Constants;
import 'in_app_browser.dart';
import 'firebase_options.dart';

final favoriteURLs = <String>{};

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Initial Local Storage
  await Preferences.init();
  // Initial Local Push Notification
  await PushNotificationService.initializeApp();
  // Initial Firebase Config
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  // Initial Downloader File
  await FlutterDownloader.initialize(
      debug: true,
      ignoreSsl: true
  );

  if (!kIsWeb &&
      kDebugMode &&
      defaultTargetPlatform == TargetPlatform.android) {
    await InAppWebViewController.setWebContentsDebuggingEnabled(kDebugMode);
  }

  runApp(const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp()));
}

class MyApp extends StatefulWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<ScaffoldMessengerState> messengerKey = GlobalKey<ScaffoldMessengerState>();


  @override
  void initState(){
    super.initState();
    // Context!
    PushNotificationService.messagesStream.listen((message) {

      print('MyApp: $message');
      //navigatorKey.currentState?.pushNamed('message', arguments: message);

      final snackBar = SnackBar(content: Text(message));
      messengerKey.currentState?.showSnackBar(snackBar);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomInAppBrowser(
        url: Constants.RUTA
        )
    );

  }
}